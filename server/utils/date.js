const moment = require('moment-timezone');

function transformTimeStamp(unix_timestamp, zone) {
    return moment
        .unix(unix_timestamp)
        .tz(zone)
        .format('HH:mm:ss');
}

module.exports = { transformTimeStamp };