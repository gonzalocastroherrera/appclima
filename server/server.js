const socketIO = require('socket.io');
const redis = require("redis");
const redisClient = redis.createClient(process.env.REDIS_URL);
const fetch = require('node-fetch');
const express = require('express');
const { transformTimeStamp } = require('./utils/date');
const { createServer } = require('http');
const port = process.env.PORT || 8000;
const app = express();
const httpServer = createServer(app);

const io = socketIO(httpServer);

httpServer.listen({ port }, () => {
    console.log('Server running');
});

io.on('connection', (client) => {
    client.on('subscribeToTimer', (interval) => {
        let cities = [
            {
                lat: -33.4372,
                long: -70.6506,
            },
            {
                lat: 47.3666687,
                long: 8.5500002
            },
            {
                lat: -36.8484597,
                long: 174.7633315
            },
            {
                lat: -33.8390007,
                long: 151.2071991
            },
            {
                lat: 51.5127907,
                long: -0.09184
            },
            {
                lat: 33.247875,
                long: -83.441162
            }
        ];
        redisClient.set("ciudades", JSON.stringify(cities));
        setInterval(() => {
            redisClient.get("ciudades", async (err, reply) => {
                try {
                    if (Math.random(0, 1) < 0.1) {
                        throw new Error('How unfortunate! The API Request Failed')
                    } else {
                        let response = [];
                        let ciudadesAry = JSON.parse(reply);
                        for (const item of ciudadesAry) {
                            let res = await fetch(`https://api.darksky.net/forecast/f04546b2e6c59a64818b3937fe453f54/${item.lat},${item.long}`);
                            let resp = await res.json();
                            let formattedTime = transformTimeStamp(resp.currently.time, resp.timezone);
                            let data = {
                                time: formattedTime,
                                temperature: resp.currently.temperature,
                                zone: resp.timezone
                            }
                            response.push(data);
                        }
                        redisClient.set("respuesta", JSON.stringify(response));
                        redisClient.get("respuesta", async (err, reply) => {
                            let respuesta = JSON.parse(reply);
                            client.emit('timer', respuesta);
                        });
                    }
                } catch (e) {
                    if (e.toString() === 'Error: How unfortunate! The API Request Failed') {
                        let date = + new Date();
                        redisClient.hset(["api.errors", date, "error al obtener la información, API request falla"]);
                        redisClient.get("respuesta", async (err, reply) => {
                            let respuesta = JSON.parse(reply);
                            client.emit('timer', respuesta);
                        });
                    }
                }
            });
        }, interval);
    });
});


