import React from 'react';

const CityList = (props) => {
    const ary = props.cities;
    const cityList = ary.map((city) => {
        return (
            <div className="col-6 text-center">
                <div className="card mb-4 shadow-sm">
                    <div className="card-header">
                        <h4 className="my-0 font-weight-normal">{city.zone}</h4>
                    </div>
                    <div className="card-body">
                        <ul className="list-unstyled mt-3 mb-4">
                            <li key={city.time.toString()}><h5>{city.time}</h5></li>
                            <li key={city.temperature.toString()}><h5>{city.temperature}°F</h5></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    });

    return (
        cityList
    );
};

export default CityList;