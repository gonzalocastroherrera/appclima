import React, { Component } from 'react';
import { subscribeToTimer } from './api/api';
import CityList from './components/citylist/CityList';
import Loader from './components/spinner/Loader';


class App extends Component {

	constructor(props) {
		super(props);

		subscribeToTimer(
			(err, ciudades) => {
				this.setState({
					cities: ciudades
				});
			}
		);
	}

	state = {
		cities: []
	};

	render() {
		if(this.state.cities.length === 0) {
			return (
			<Loader />
			)
		}
		return (
			<div className="container pt-md-5" style={{marginTop: 15+"px"}}>
				<div className="row">
					<CityList cities={this.state.cities} />
				</div>
			</div>
		);
	}
}

export default App;